from threading import Thread
import queue # For queuing 
import requests # For sending requests
import urllib # To format wiki request
import json # For parsing, sending and receiving json
import spacy # For NLP  #nlp = spacy.load('en_core_web_sm')
import sys, getopt
from time import sleep

_TERMINATE = False
_NLP = spacy.load('en_core_web_sm')


class Harvest:
    def __init__(self, query, depth):
        self.query = query
        self.depth = depth
        self.text = ""
        self.entites = []

class WikiHarvest:
    def __init__(self):
        self.SESSION = requests.Session()
        self.URL = "https://en.wikipedia.org/w/api.php"

    def SearchWikiArticles(self, query):
        parameters = {
            "action": "opensearch",
            "search": query,
            "limit": "5",
            "format": "json"
        }
        response = self.SESSION.get(url=self.URL, params=parameters)
        data = response.json()
        return data[1]

    def SearchWikiTitle(self, query):
        parameters = {
            "action": "query",
            "list": "search",
            "srsearch": query,
            "utf8": "",
            "srprop": "titlesnippet",
            "format": "json"
        }
        response = self.SESSION.get(url=self.URL, params=parameters)
        data = response.json()
        return data
        
    def GetArticleWiki(self, query):
        URL = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&exintro&explaintext&titles="
        query = urllib.parse.quote(query)
        URL = URL + query
        response = self.SESSION.get(url=self.URL)
        data = response.json()
        # Clean
        # Removing all parenthesis
        for page in data["query"]["pages"]:
            data = data["query"]["pages"][page]["extract"]
            start = 0
            while start != -1:
                start = data.find('(')
                end = data.find(')')
                if start != -1 and end != -1:
                    data = data[:start-1] + data[end+1:]
            break
        return data

    def GetEntities(self, string):
        global _NLP
        nlp = _NLP(string)

    def Run(self, query):
        terms = self.GetArticleWiki(query)[0]
        return terms


                


def Worker(q):
        while True:
            item = q.get()
            if item == None:
                break
            wikiHarvest = WikiHarvest()
            harvest = wikiHarvest.Run(item.query)

            q.task_done()

def Start(query, depth, t_count):
    q = queue.Queue()
    item = Harvest(query, depth)
    q.put(item)
    threads = []
    for i in range(t_count):
        t = Thread(target=Worker, args =(q,))
        t.setDaemon(True)
        t.start()
        threads.append(t)

    # block until all tasks are done
    q.join()
    for i in range(t_count):
        q.put(None)

    # stop workers
    for t in threads:
        t.join()
    print("cool")

def main():
    query = None
    depth = 2
    threads = 10
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hqt",["help=","query=","depth=","threads="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-q", "--query"):
            query = a
        elif o in ("-d", "--depth"):
            depth = int(a)
        elif o in ("-t", "--threads"):
            threads = int(a)
        else:
            assert False, "unhandled option"
    
    if query != None and depth > 1 and threads > 0:
        Start(query, depth, threads)
    elif query == None or "":
        assert False, "No query provided"
        sys.exit()
    else:
        assert False, "Something went wrong"
        sys.exit()
    
def usage():
    print("""
        This tool will create a knowledge graph for a given query.

        Default:
            query: None
            depth: 2
            threads: 10
    """)

if __name__ == "__main__":
    main()

